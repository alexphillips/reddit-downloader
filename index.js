let App = require('node-climax').App;
(new App('redditdl'))
  .init({
    'dl:sub': {
      usage: '',
      desc: 'Get Your Music',
      options: {
        s: {
          alias: 'save-dir',
          demand: false,
          desc: 'Save directory',
          type: 'string',
        },
        c: {
          alias: 'title-contains',
          demand: false,
          desc: 'Only download posts where the title contains...',
          type: 'string',
        },
        l: {
          alias: 'limit',
          demand: false,
          desc: 'Limit number of simultaneous downloads',
          type: 'string',
        },
      },
      file: `${__dirname}/DownloadSubreddit`,
    },
  })
  .run();
