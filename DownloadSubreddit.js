let RedditCommand = require('./RedditCommand'),
  Logger = require('node-climax').Logger,
  Request = require('request-promise-native'),
  cheerio = require('cheerio'),
  path = require('path'),
  fs = require('fs-extra'),
  async = require('async');

class DownloadSubreddit extends RedditCommand {
  async run (args, options) {
    let response = null;

    this.titleContains = options['title-contains'] ? options['title-contains'] : null;
    this.limit = options.limit ? options.limit : 10;

    for (let sub of args) {
      this.saveDir = RedditCommand.trimPath(options['save-dir'] ? options['save-dir'] : '/tmp/');
      this.saveDir = `${this.saveDir}/${sub}`;

      try {
        fs.mkdirpSync(this.saveDir);
      } catch (e) {}

      this.metrics = {
        posts: 0,
        downloaded: 0,
        skipped: 0,
        existing: 0,
      };

      let prevId = null;
      do {
        let url = `http://reddit.com/r/${sub}.json`;
        if (prevId) {
          url += `?after=t3_${prevId}`;
        }

        Logger.debug(`Fetching reddit posts from ${url}`);
        let data = await Request.get(url);
        response = JSON.parse(data);

        if (!response) {
          Logger.error(data);
          throw Error(`Failed to retrieve post data from ${url}`);
        }

        Logger.debug(`Got posts response: ${data}`);

        if (response.data.children.length !== 0) {
          Logger.debug(`Processing ${response.data.children.length} posts`);
          for (let post of response.data.children) {
            await this.downloadPost(post);
          }

          prevId = response.data.children.pop().data.id;
        }
      } while(response.data.children && response.data.children.length > 0);

      Logger.info(`Processed ${this.metrics.posts} posts`);
    }
  }

  async downloadPost(post) {
    this.metrics.posts++;
    if (this.titleContains && !post.data.title.match(new RegExp(this.titleContains, 'i'))) {
      return;
    }

    Logger.info(`Processing post ${post.data.title}`);

    try {
      var response = JSON.parse(await Request.get(`http://reddit.com${post.data.permalink}.json`));
    } catch (err) {
      return Logger.error(`Error fetching URL http://reddit.com${post.data.permalink}.json: ${err}`);
    }

    post = response[0].data.children[0];
    let comments = response[1].data.children;

    Logger.verbose(`Fetching post URL(s): ${post.data.url}`);

    try {
      var urls = await this.extractUrls(post.data.url);
    } catch (e) {
      Logger.error(`Failed to extract URLs for post ${post.data.title} (${post.data.url})`);
      throw Error(e);
    }

    Logger.debug(`Found ${urls.length} URLs in post: ${JSON.stringify(urls)}`);
    Logger.verbose(`Fetching comment URL(s)`);

    for (let comment of comments) {
      let regex = new RegExp("[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?|gfycat\.com/\w+", "gi"),
        match = null,
        commentUrls = [];
      while (match = regex.exec(comment.data.body)) {
        if (!match[1]) {
          continue;
        }

        Logger.debug(`Found URL in comment: ${match[1]}`);
        Logger.debug(`Comment body: ${comment.data.body}`);
        commentUrls = commentUrls.concat(await this.extractUrls(match[1]));
      }

      urls = urls.concat(commentUrls);
    }

    Logger.verbose(`Found ${urls.length} total URL(s)`);

    let fileTemplate = `${this.saveDir}/${RedditCommand.slugify(post.data.title)} (${post.data.id}) - {{NUM}}`;

    return new Promise((resolve, reject) => {
      async.forEachOfLimit(urls, this.limit, (url, index, callback) => {
        if (!url) {
          Logger.error(`Invalid URL found from post ${post.data.title}`);
          return callback();
        }

        this.downloadFile(url, fileTemplate.replace('{{NUM}}', index))
          .then(callback)
          .catch(callback);
      }, err => {
        if (err) {
          return reject(err);
        }

        return resolve();
      });
    });
  }

  async extractUrls(url) {
    if (url.match(/^\/a\//) || url.match(/^\/gallery\//)) {
      url = `http://imgur.com${url}`;
    }

    if (url.match(/imgur\.com/)) {
      return await this.extractImgurUrls(url);
    }

    if (url.match(/redd\.it.+?(?:jpg|gif|png|jpeg)/)) {
      return [url];
    }

    // Array of known good image URLs
    if (url.match(new RegExp([
      'thechive\.files\.wordpress\.com',
      'media\.tumblr\.com',
      'sli\.mg',
      'static\.qnm\.it',
    ].join('|')))) {
      return [url];
    }

    if (url.match(/gfycat\.com/)) {
      return this.extractGfycatUrl(url);
    }

    Logger.error(`Unsupported URL: ${url}`);

    return [];
  }

  async extractGfycatUrl(url) {
    while (url.charAt(url.length - 1) === '/') {
      url = url.substring(0, url.length - 1);
    }

    let identifier = url.split('/').pop();

    try {
      Logger.debug(`Attempting to fetch gfycat URL with identifier ${identifier}`);
      let response = await Request.get(`http://gfycat.com/cajax/get/${identifier}`);
      response = JSON.parse(response);
      Logger.debug(`gfycat response: ${JSON.stringify(response)}`);

      if (response.mp4Url) {
        return [response.mp4Url];
      } else if (response.gfyItem && response.gfyItem.mp4Url) {
        return [response.gfyItem.mp4Url];
      }

      Logger.error('Invalid Gfycat response');

      return [];
    } catch (e) {
      throw Error(`Error with url ${url}: ${e}`);
    }
  }

  async extractImgurUrls(url) {
    Logger.verbose(`Detected imgur URL: ${url}`);
    if (url.match(/imgur\.com\/a\//) || url.match(/imgur\.com\/gallery\//)) {
      return await this.extractImgurAlbumUrls(url);
    }

    try {
      var response = await Request.get(url);
    } catch (err) {
      if (err.statusCode === 404) {
        Logger.verbose(`URL ${url} not found. Skipping.`);

        return [];
      }

      Logger.error(`ERROR fetching URL ${url}`);
      throw err;
    }

    let $ = cheerio.load(response);

    if ($('div.video-container').get() && $('div.video-container').get().length) {
      Logger.verbose(`URL appears to be a video`);
      let url = $('div.video-container source').first().attr('src');
      if (url.match(/^\/\//)) {
        url = `http:${url}`;

        return [url];
      }
    }

    if (url.match(/\.png$/)) {
      url = url.replace(/\.png$/, '.jpg');
    } else {
      let ext = path.extname(url);
      if (ext == '.gifv') {
        url = url.replace(/\.gifv$/, '.gif');
      }
      if (!ext) {
        url = `${url}.jpg`;
      }
    }

    return [url];
  }

  async extractImgurAlbumUrls(url) {
    Logger.verbose(`Detected imgur album URL: ${url}`);

    let regex = /.*?"hash":"([a-zA-Z0-9]+)".*?"ext":"(\.[a-zA-Z0-9]+)".*?/g,
      match = null,
      retval = [];

    try {
      var response = await Request.get(url);
    } catch (err) {
      if (err.statusCode === 404) {
        Logger.verbose(`URL ${url} not found. Skipping.`);

        return [];
      }

      Logger.error(`ERROR fetching URL ${url}`);
      throw err;
    }

    while (match = regex.exec(response)) {
      retval.push(`http://i.imgur.com/${match[1]}${match[2]}`);
    }

    return retval;
  }

  downloadFile(url, filepath) {
    return new Promise((resolve, reject) => {
      Logger.debug(`Extracting extension from ${url}`);
      // Trim possible query string from URL
      let ext = path.extname(require('url').parse(url).pathname);
      filepath += ext;

      Request.head(url, {
        resolveWithFullResponse: true,
      })
        .then(head => {
          if (fs.existsSync(filepath)) {
            let localStat = fs.statSync(filepath);
            if (localStat.size == head.headers['content-length']) {
              Logger.info(`File ${filepath} exists and is the same size. Skipping.`);

              return resolve();
            }

            Logger.warn(`File ${filepath} exists but is NOT the correct size. Re-downloading.`);
          }

          Logger.verbose(`Downloading ${filepath} from ${url}`);

          let writeStream = fs.createWriteStream(`${filepath}.dl`);
          let stream = Request.get(url)
            .on('error', err => {
              Logger.error(`ERROR attempting to stream file ${url}`);
              if (err.statusCode === 404) {
                Logger.verbose(`URL ${url} not found. Skipping.`);
                return resolve();
              }

              return reject(err);
            })
            .pipe(writeStream);

          stream.on('finish', err => {
            writeStream.close();
            if (err) {
              return reject(err);
            }

            fs.renameSync(`${filepath}.dl`, filepath);

            return resolve();
          });
        })
        .catch(err => {
          if (err.response.statusCode) {
            Logger.error(`URL ${url} returned with status code 404. Skipping`);
            return resolve();
          }

          return reject(err);
        });
    });
  }
}

module.exports = DownloadSubreddit;
