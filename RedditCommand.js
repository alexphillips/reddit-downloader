let Command = require('node-climax').Command;

class RedditCommand extends Command {
  static trimPath(string) {
    while (string.charAt(string.length - 1) === '/') {
      string = string.substring(0, string.length - 1);
    }

    return string;
  }

  static slugify(text) {
    let retval = text.toString().toLowerCase()
      .replace(/\s+/g, '-')           // Replace spaces with -
      .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
      .replace(/\-\-+/g, '-')         // Replace multiple - with single -
      .replace(/^-+/, '')             // Trim - from start of text
      .replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '') // remove emojis
      .replace(/-+$/, '');            // Trim - from end of text

    if (retval.length > 200) {
      retval = retval.substring(0, 200) + '...';
    }

    return retval;
  }
}

module.exports = RedditCommand;
